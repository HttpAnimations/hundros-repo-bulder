const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const path = require('path');
const fs = require('fs-extra');

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'src', 'renderer.js'),
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  mainWindow.loadFile('src/index.html');
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

ipcMain.handle('create-mod', async (event, modName) => {
  const modPath = path.join(__dirname, modName);
  const templatePath = path.join(__dirname, 'ModEXP');

  try {
    await fs.copy(templatePath, modPath);
    return `Mod "${modName}" created successfully.`;
  } catch (err) {
    console.error(err);
    throw new Error(`Failed to create mod: ${err.message}`);
  }
});
