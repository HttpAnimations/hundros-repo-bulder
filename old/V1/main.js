const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const path = require('path');
const fs = require('fs');

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  mainWindow.loadFile('index.html');

  const menu = Menu.buildFromTemplate([
    {
      label: 'View',
      submenu: [
        {
          label: 'Live Experience',
          click() {
            mainWindow.webContents.send('load-page', 'liveEXP.html');
          }
        },
        {
          label: 'GUI Maker',
          click() {
            mainWindow.webContents.send('load-page', 'guiMaker.html');
          }
        }
      ]
    }
  ]);

  Menu.setApplicationMenu(menu);

  mainWindow.on('closed', function () {
    mainWindow = null;
  });
}

ipcMain.handle('load-json', async () => {
  const filePath = path.join(__dirname, 'userRepo.json');
  const data = fs.readFileSync(filePath, 'utf-8');
  return JSON.parse(data);
});

ipcMain.handle('save-json', async (event, json) => {
  const filePath = path.join(__dirname, 'userRepo.json');
  fs.writeFileSync(filePath, JSON.stringify(json, null, 2), 'utf-8');
  return true;
});

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});
