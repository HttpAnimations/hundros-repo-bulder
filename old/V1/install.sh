#!/bin/bash

# Check if npm is installed
if command -v npm > /dev/null 2>&1; then
    echo "Installing"
    npm install
    echo "Done :3"
else
    echo "npm is not installed. Please install Node.js from https://nodejs.org/"
    read -p "Would you like to open the Node.js webpage now? (y/n): " open_webpage
    if [ "$open_webpage" = "y" ]; then
        # Open the Node.js webpage in the default web browser
        if command -v xdg-open > /dev/null 2>&1; then
            xdg-open https://nodejs.org/
        elif command -v open > /dev/null 2>&1; then
            open https://nodejs.org/
        elif command -v start > /dev/null 2>&1; then
            start https://nodejs.org/
        else
            echo "Please open https://nodejs.org/ in your web browser to download and install Node.js."
        fi
    else
        echo "Please visit https://nodejs.org/ to download and install Node.js."
    fi
fi
