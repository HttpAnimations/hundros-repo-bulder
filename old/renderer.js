const monaco = require('monaco-editor');

document.addEventListener('DOMContentLoaded', () => {
  monaco.editor.create(document.getElementById('editor'), {
    value: '// Start coding...',
    language: 'javascript'
  });
});
