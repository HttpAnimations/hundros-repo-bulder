// Function to fetch and initialize JSON data from local storage or initialize if not present
function initLocalStorage() {
    const initialData = {
        Torrents: [
            {
                "Name": "NAMEOFTORRENT",
                ".Torrent": "DOWNLOADFORTORRENT",
                "MagnetUrl": "MagnetURL",
                "Icon": "https://github.com/HttpAnimation/RepoNEW/blob/main/REPLACEME?raw=true",
                "SeedersAtTheTime": "9",
                "Publisher": "WHOMADETHETORRENTFILE",
                "HasStreamURL": false,
                "StreamURL": "IFYOUCANSTREAMTHESHOWTHENADDTHEURLHERE",
                "Source": "WHERETHETORRENTCAMEOVER"
            }
        ]
    };

    // Check if data exists in local storage
    let jsonData = localStorage.getItem('torrentsData');
    if (!jsonData) {
        // Initialize local storage with initial data
        localStorage.setItem('torrentsData', JSON.stringify(initialData));
        jsonData = JSON.stringify(initialData);
    }

    return JSON.parse(jsonData);
}

// Function to save JSON data to local storage
function saveToLocalStorage(data) {
    localStorage.setItem('torrentsData', JSON.stringify(data));
}

// Function to export JSON data as a file
function exportJSON(data) {
    const jsonData = JSON.stringify(data, null, 2);
    const blob = new Blob([jsonData], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'torrents.json';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
}

// Function to initialize the app
function initApp() {
    const form = document.getElementById('torrentForm');
    const addItemButton = document.getElementById('addItemButton');
    const exportButton = document.getElementById('exportButton');

    // Fetch initial data from local storage
    let data = initLocalStorage();

    // Populate form fields with initial data
    const torrent = data.Torrents[0]; // Assuming only one torrent for simplicity
    document.getElementById('name').value = torrent.Name;
    document.getElementById('torrent').value = torrent[".Torrent"];
    document.getElementById('magnetUrl').value = torrent.MagnetUrl;
    document.getElementById('icon').value = torrent.Icon;
    document.getElementById('seeders').value = torrent.SeedersAtTheTime;
    document.getElementById('publisher').value = torrent.Publisher;
    document.getElementById('hasStream').checked = torrent.HasStreamURL;
    document.getElementById('streamUrl').value = torrent.StreamURL;
    document.getElementById('source').value = torrent.Source;

    // Event listener for form submission
    form.addEventListener('submit', function(event) {
        event.preventDefault();
        const formData = new FormData(form);
        const newData = {
            Torrents: [{
                Name: formData.get('name'),
                ".Torrent": formData.get('torrent'),
                MagnetUrl: formData.get('magnetUrl'),
                Icon: formData.get('icon'),
                SeedersAtTheTime: formData.get('seeders'),
                Publisher: formData.get('publisher'),
                HasStreamURL: formData.get('hasStream') ? true : false,
                StreamURL: formData.get('streamUrl'),
                Source: formData.get('source'),
            }]
        };
        
        // Save updated data to local storage
        saveToLocalStorage(newData);
        alert('Data saved successfully!');
    });

    // Event listener for add new item button
    addItemButton.addEventListener('click', function() {
        const newTorrent = {
            Name: '',
            ".Torrent": '',
            MagnetUrl: '',
            Icon: '',
            SeedersAtTheTime: '',
            Publisher: '',
            HasStreamURL: false,
            StreamURL: '',
            Source: ''
        };
        data.Torrents.push(newTorrent);
        saveToLocalStorage(data);

        // Reset form fields to empty values
        document.getElementById('name').value = '';
        document.getElementById('torrent').value = '';
        document.getElementById('magnetUrl').value = '';
        document.getElementById('icon').value = '';
        document.getElementById('seeders').value = '';
        document.getElementById('publisher').value = '';
        document.getElementById('hasStream').checked = false;
        document.getElementById('streamUrl').value = '';
        document.getElementById('source').value = '';

        alert('New item added successfully!');
    });

    // Event listener for export button
    exportButton.addEventListener('click', function() {
        const jsonData = localStorage.getItem('torrentsData');
        const parsedData = JSON.parse(jsonData);
        exportJSON(parsedData);
    });
}

// Initialize the app
initApp();
